import express from 'express';

import {
  CreateUser,
  LoginUser
} from '../controllers/authController'
import {
  validateRegisterBody,
  validateLoginBody
} from '../utils/utils_auth'

const router = express.Router();

router.post('/register', validateRegisterBody, CreateUser);
router.post('/login', validateLoginBody, LoginUser);

export default router;

