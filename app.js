import express from 'express';
import morgan from 'morgan';
import compression from 'compression';
import cors from 'cors';

// Routes
import userRouter from './routes/usersRoutes'

// Express server
const app = express();

// 1) Middleware
if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'));
}

// Express Configuration
app.use(compression());
app.use(express.json());
app.use(cors());
app.options('*', cors());


// Rutas REST
app.get('/', (req, res) => {
  res.json({
    msg: 'Welcome to the server'
  })
});

app.use('/api/v1/auth', userRouter);

export default app;