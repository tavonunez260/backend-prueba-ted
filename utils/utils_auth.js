import { get } from 'lodash'

export const validateEmail = async (email) => {
  return /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test(email);
};

export const validatePassword = async (password) => {
  return /^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{8,})/.test(password);
}

export const validateRegisterBody = async (req, res, next) => {
  // Verifica que tenga todos los paramatetros requeridos
  const messages = [];
  const requireds = [
    'email',
    'name',
    'password',
    'passwordConfirm',
  ];

  requireds.map(item => {
    if (!get(req.body, item, false)) {
      messages.push(`El parámetro "${item}" es requerido`);
    }
  });

  if (messages.length) {
    res.status(400).json({
      message: messages
    });
  }

  const isEmailValid = await validateEmail(req.body.email);

  if (!isEmailValid) {
    res.status(400).json({
      message: 'El correo electrónico no tiene un formato válido'
    });
  }

  const isPasswordValid = await validatePassword(req.body.password);
  if (!isPasswordValid) {
    res.status(400).json({
      message: 'La contraseña no tiene un formato válido, debe contener al menos 1 letra mayúscula, 1 letra minúscula, 1 número, 1 carácter especial y un longitud de al menos 8 caracteres'
    });
  }

  next();
};

export const validateLoginBody = async (req, res, next) => {
  // Verifica que tenga todos los paramatetros requeridos
  const messages = [];
  const requireds = [
    'email',
    'password',
  ];

  requireds.map(item => {
    if (!get(req.body, item, false)) {
      messages.push(`El parámetro "${item}" es requerido`);
    }
  });

  if (messages.length) {
    res.status(400).json({
      message: messages
    });
  }

  const isEmailValid = await validateEmail(req.body.email);

  if (!isEmailValid) {
    res.status(400).json({
      message: 'El parámetro "email" debe ser de un correo electrónico válido'
    });
  }
  next();
};