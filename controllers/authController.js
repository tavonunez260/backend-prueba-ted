import UserSchema from '../models/userModel';
import { validateEmail, validatePassword } from '../utils/utils_auth'

import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'

export const CreateUser = async (req, res) => {
  try {
    if (req.body.password !== req.body.passwordConfirm) {
      throw new Error('Contraseñas diferentes')
    }

    const salt = await bcrypt.genSaltSync(10);
    const hash = await bcrypt.hashSync(req.body.password, salt);

    const userModel = {
      name: req.body.name,
      email: req.body.email,
      password: hash
    }
    const isEmailValid = await validateEmail(req.body.email);
    const isPasswordValid = await validatePassword(req.body.password);

    if (isEmailValid && isPasswordValid) {
      const newUser = await UserSchema.create(userModel);
      newUser.password = ''
      const token = jwt.sign({ id: newUser._id }, process.env.JWT_SECRET, {
        expiresIn: process.env.JWT_EXPIRES_IN
      });
      res.status(201).json({
        status: 'success',
        msg: 'Usuario creado satisfactoriamente',
        data: {
          token: token,
          user: newUser
        }
      })
    }

  } catch (err) {
    if (err.message.includes('E11000 duplicate key error')) {
      res.status(400).json({
        status: 'fail',
        message: 'Ya existe un usuario registrado con este correo'
      });
    }
    res.status(400).json({
      status: 'fail',
      message: err.message
    });
  }
}

export const LoginUser = async (req, res) => {
  try {
    const { email, password } = req.body

    // FIND USER
    const user = await UserSchema.findOne({
      email
    }).select('+password')
    if (!user) throw new Error('No existe un usuario registrado con este correo')

    const isEqual = await bcrypt.compareSync(
      password,
      user.password
    );
    if (!isEqual) throw new Error('Contraseña incorrecta')

    const token = jwt.sign({ id: user._id }, process.env.JWT_SECRET, {
      expiresIn: process.env.JWT_EXPIRES_IN
    });

    res.status(201).json({
      status: 'success',
      user: {
        name: user.name
      },
      token: token
    })
  } catch (err) {
    res.status(400).json({
      status: 'fail',
      message: err.message
    });
  }
}