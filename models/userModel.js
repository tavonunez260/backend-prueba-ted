import mongoose from 'mongoose';
import softDelete from 'mongoosejs-soft-delete'
import validator from 'validator'

const Schema = mongoose.Schema;

const userSchema = new Schema({
  name: {
    type: String,
    required: [true, 'Falta el campo nombre'],
    validate: [validator.isAlpha, 'Nombre solo debe llevar letras']
  },
  email: {
    type: String,
    required: [true, 'Falta el campo email'],
    unique: true,
    lowercase: true,
    validate: [validator.isEmail, 'Este email no es valido']
  },
  password: {
    type: String,
    required: [true, 'Falta el campo contraseña'],
    minlength: 8,
    select: false
  }
}, {
  timestamps: true
});

userSchema.plugin(softDelete);

const User = mongoose.model('user', userSchema);

export default User;