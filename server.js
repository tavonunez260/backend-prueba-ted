import 'dotenv/config';
import mongoose from 'mongoose';
import app from './app';

const DB = process.env.DATABASE.replace(
  '<PASSWORD>',
  process.env.DATABASE_PASSWORD
);

mongoose
  .connect(DB, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log(`DB connection successful. Conected to: ${DB}`)
  })
  .catch(err => {
    console.log(`Error on db connection. Conected to: ${err}`)
    mongoose.disconnect();
  });

const port = process.env.PORT || 3030;

const server = app.listen(port, () => {
  console.log(`App running on port ${port}, http://localhost:${port}`)
});

export default server;